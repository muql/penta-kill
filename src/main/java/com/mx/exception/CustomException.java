package com.mx.exception;

/**
 * 自定义异常(CustomException)
 * @author muql
 * @date 2020/3/25 18:33
 */
public class CustomException extends RuntimeException {

    public CustomException(String msg){
        super(msg);
    }

    public CustomException() {
        super();
    }
}
