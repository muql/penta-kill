package com.mx.exception;

/**
 * 自定义401无权限异常(UnauthorizedException)
 * @author muql
 * @date 2020/3/25 18:33
 */
public class CustomUnauthorizedException extends RuntimeException {

    public CustomUnauthorizedException(String msg){
        super(msg);
    }

    public CustomUnauthorizedException() {
        super();
    }
}
