package com.mx.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mx.model.entity.Permission;

/**
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@TableName(value = "permission")
public class PermissionDto extends Permission {

}