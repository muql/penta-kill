package com.mx.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mx.model.entity.UserRole;

/**
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@TableName(value = "user_role")
public class UserRoleDto extends UserRole {

}