package com.mx.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mx.model.entity.RolePermission;

/**
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@TableName(value= "role_permission")
public class RolePermissionDto extends RolePermission {

}