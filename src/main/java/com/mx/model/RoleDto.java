package com.mx.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mx.model.entity.Role;

/**
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@TableName(value = "role")
public class RoleDto extends Role {

}