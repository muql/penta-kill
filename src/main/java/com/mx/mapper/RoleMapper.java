package com.mx.mapper;

import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.mx.model.RoleDto;
import com.mx.model.entity.User;

import java.util.List;

/**
 * RoleMapper
 * @author muql
 * @date 2020/3/25 18:33
 */
public interface RoleMapper extends Mapper<RoleDto> {
    /**
     * 根据User查询Role
     * @param userDto
     * @return java.util.List<com.wang.model.RoleDto>
     * @author dolyw.com
     * @date 2018/8/31 11:30
     */
    List<RoleDto> findRoleByUser(User userDto);
}