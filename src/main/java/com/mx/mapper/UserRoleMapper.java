package com.mx.mapper;

import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.mx.model.UserRoleDto;

/**
 * UserRoleMapper
 * @author muql
 * @date 2020/3/25 18:33
 */
public interface UserRoleMapper extends Mapper<UserRoleDto> {
}