package com.mx.mapper;

import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.mx.model.PermissionDto;
import com.mx.model.RoleDto;

import java.util.List;

/**
 * PermissionMapper
 * @author muql
 * @date 2020/3/25 18:33
 */
public interface PermissionMapper extends Mapper<PermissionDto> {
    /**
     * 根据Role查询Permission
     * @param roleDto
     * @return java.util.List<com.wang.model.PermissionDto>
     * @author dolyw.com
     * @date 2018/8/31 11:30
     */
    List<PermissionDto> findPermissionByRole(RoleDto roleDto);
}