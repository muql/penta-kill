package com.mx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mx.model.entity.User;

/**
 * UserMapper
 * @author muql
 * @date 2020/3/25 18:33
 */
public interface UserMapper extends BaseMapper<User> {
}