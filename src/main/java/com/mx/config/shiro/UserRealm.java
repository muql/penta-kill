package com.mx.config.shiro;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mx.config.shiro.jwt.JwtToken;
import com.mx.model.entity.User;
import com.mx.util.JedisUtil;
import com.mx.mapper.PermissionMapper;
import com.mx.mapper.RoleMapper;
import com.mx.mapper.UserMapper;
import com.mx.model.common.Constant;
import com.mx.util.JwtUtil;
import com.mx.util.common.MD5Util;
import com.mx.util.common.StringUtil;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 自定义Realm
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@Service
public class UserRealm extends AuthorizingRealm {
    @Autowired
    ShiroConfigProperties shiroConfigProperties;
    /**
     * RefreshToken过期时间
     */
    private final UserMapper userMapper;
    private final RoleMapper roleMapper;
    private final PermissionMapper permissionMapper;

    @Autowired
    public UserRealm(UserMapper userMapper, RoleMapper roleMapper, PermissionMapper permissionMapper) {
        this.userMapper = userMapper;
        this.roleMapper = roleMapper;
        this.permissionMapper = permissionMapper;
    }

    /**
     * 大坑，必须重写此方法，不然Shiro会报错
     */
    @Override
    public boolean supports(AuthenticationToken authenticationToken) {
        return (authenticationToken instanceof JwtToken)||(authenticationToken instanceof UsernamePasswordToken);
    }

    /**
     * 只有当需要检测用户权限的时候才会调用此方法，例如checkRole,checkPermission之类的
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
//        String account = JwtUtil.getClaim(principalCollection.toString(), Constant.ACCOUNT);
//        UserDto userDto = new UserDto();
//        userDto.setAccount(account);
//        // 查询用户角色
//        List<RoleDto> roleDtos = roleMapper.findRoleByUser(userDto);
//        for (RoleDto roleDto : roleDtos) {
//            if (roleDto != null) {
//                // 添加角色
//                simpleAuthorizationInfo.addRole(roleDto.getName());
//                // 根据用户角色查询权限
//                List<PermissionDto> permissionDtos = permissionMapper.findPermissionByRole(roleDto);
//                for (PermissionDto permissionDto : permissionDtos) {
//                    if (permissionDto != null) {
//                        // 添加权限
//                        simpleAuthorizationInfo.addStringPermission(permissionDto.getPerCode());
//                    }
//                }
//            }
//        }
        return simpleAuthorizationInfo;
    }

    /**
     * 默认使用此方法进行用户名正确与否验证，错误抛出异常即可。
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        if (authenticationToken instanceof JwtToken) {
            String token = (String) authenticationToken.getCredentials();
            // 解密获得account，用于和数据库进行对比
            String account = JwtUtil.getClaim(token, Constant.ACCOUNT);
            // 帐号为空
            if (StringUtil.isBlank(account)) {
                throw new AuthenticationException("Token中帐号为空(The account in Token is empty.)");
            }
//            // 查询用户是否存在
//            UserDto userDto = new UserDto();
//            userDto.setAccount(account);
//            userDto = userMapper.selectOne(userDto);
//            if (userDto == null) {
//                throw new AuthenticationException("该帐号不存在(The account does not exist.)");
//            }

            // 判断当前帐号是否在RefreshToken过渡时间，是就放行
            if (JedisUtil.exists(Constant.PREFIX_SHIRO_REFRESH_TOKEN_TRANSITION + account)) {
                // 获取RefreshToken过渡时间Key保存的旧Token
                String oldToken = JedisUtil.getObject(Constant.PREFIX_SHIRO_REFRESH_TOKEN_TRANSITION + account).toString();
                // 判断旧Token是否一致
                if (token.equals(oldToken)) {
                    return new SimpleAuthenticationInfo(oldToken, oldToken, "userRealm");
                }
            }
            // 开始认证，要AccessToken认证通过，且Redis中存在RefreshToken，且两个Token时间戳一致
            if (JwtUtil.verify(token) && JedisUtil.exists(Constant.PREFIX_SHIRO_REFRESH_TOKEN + account)) {
                // 获取RefreshToken的时间戳
                String currentTimeMillisRedis = JedisUtil.getObject(Constant.PREFIX_SHIRO_REFRESH_TOKEN + account).toString();

                // 获取AccessToken时间戳，与RefreshToken的时间戳对比
                if (JwtUtil.getClaim(token, Constant.CURRENT_TIME_MILLIS).equals(currentTimeMillisRedis)) {
                    return new SimpleAuthenticationInfo(token, token, "userRealm");
                }
            }
            throw new AuthenticationException("Token已过期(Token expired or incorrect.)");
        } else if (authenticationToken instanceof UsernamePasswordToken) {
            // 查询用户是否存在
            User userDto = new User();
            char[] password = ((UsernamePasswordToken) authenticationToken).getPassword();
            userDto.setAccount(((UsernamePasswordToken) authenticationToken).getUsername());
            userDto = userMapper.selectOne(new QueryWrapper<User>().eq("account",userDto.getAccount()));
            if (userDto == null) {
                throw new UnknownAccountException ("该帐号不存在(The account does not exist.)");
            }

            String key = MD5Util.getMD5(String.valueOf(password));
            // 因为密码加密是以帐号+密码的形式进行加密的，所以解密后的对比是帐号+密码
            if (key.equals(userDto.getPassword())) {
                // 清除可能存在的Shiro权限信息缓存
                if (JedisUtil.exists(Constant.PREFIX_SHIRO_CACHE + userDto.getAccount())) {
                    JedisUtil.delKey(Constant.PREFIX_SHIRO_CACHE + userDto.getAccount());
                }
                // 设置RefreshToken，时间戳为当前时间戳，直接设置即可(不用先删后设，会覆盖已有的RefreshToken)
                String currentTimeMillis = String.valueOf(System.currentTimeMillis());
                JedisUtil.setObject(Constant.PREFIX_SHIRO_REFRESH_TOKEN + userDto.getAccount(), currentTimeMillis, Integer.parseInt(shiroConfigProperties.getRefreshTokenExpireTime()));
                // 从Header中Authorization返回AccessToken，时间戳为当前时间戳
                String token = JwtUtil.sign(userDto.getAccount(), currentTimeMillis);
                System.out.println(token);
                ((UsernamePasswordToken) authenticationToken).setPassword(token.toCharArray());
                return new SimpleAuthenticationInfo(token, token, "userRealm");
            } else {
                throw new IncorrectCredentialsException ("帐号或密码错误(Account or Password Error.)");
            }
        }
        throw new AuthenticationException ("未知错误)");
    }
}
