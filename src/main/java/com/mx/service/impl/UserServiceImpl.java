package com.mx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mx.mapper.UserMapper;
import com.mx.model.entity.User;
import com.mx.service.IUserService;
import org.springframework.stereotype.Service;

/**
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
}
