package com.mx.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mx.model.entity.User;

/**
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
public interface IUserService extends IService<User> {
}
