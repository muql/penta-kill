package com.mx.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mx.config.shiro.cache.CustomCache;
import com.mx.exception.CustomException;
import com.mx.exception.CustomUnauthorizedException;
import com.mx.model.common.BaseDto;
import com.mx.model.common.Constant;
import com.mx.model.common.ResponseBean;
import com.mx.model.entity.User;
import com.mx.model.valid.group.UserEditValidGroup;
import com.mx.model.valid.group.UserLoginValidGroup;
import com.mx.service.IUserService;
import com.mx.util.JedisUtil;
import com.mx.util.UserUtil;
import com.mx.util.common.MD5Util;
import com.mx.util.common.RespUtils;
import com.mx.util.common.StringUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * UserController
 *
 * @author muql
 * @date 2020/3/25 18:33
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserUtil userUtil;

    private final IUserService userService;

    @Autowired
    public UserController(UserUtil userUtil, IUserService userService) {
        this.userUtil = userUtil;
        this.userService = userService;
    }

    /**
     * 获取用户列表
     *
     * @param
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author muql
     * @date 2018/8/30 10:41
     */
    @RequiresAuthentication
    @GetMapping
    public Object user(@Validated BaseDto baseDto) {
        if (baseDto.getPage() == null || baseDto.getRows() == null) {
            baseDto.setPage(1);
            baseDto.setRows(10);
        }
        Page<User> page = new Page<User>(baseDto.getPage(), baseDto.getRows());//第一页，每页5条
        IPage<User> userDtos = userService.page(page);
        if (userDtos.getRecords() == null || userDtos.getRecords().size() < 0) {
            throw new CustomException("查询失败(Query Failure)");
        }
        Map<String, Object> result = new HashMap<String, Object>(16);
        result.put("count", userDtos.getTotal());
        result.put("data", userDtos.getRecords());
        return RespUtils.success(result, "查询成功(Query was successful)");
    }

    @GetMapping("/toSignUp")
    public Object toSignUp() {
        ModelAndView modelAndView = new ModelAndView("/page/signup.html");
        return modelAndView;
    }

    @GetMapping("/home")
    public Object home() {
        ModelAndView modelAndView = new ModelAndView("/page/home.html");
        return modelAndView;
    }

    @GetMapping("/index")
    public Object index() {
        ModelAndView modelAndView = new ModelAndView("/index.html");
        return modelAndView;
    }

    /**
     * 获取在线用户(查询Redis中的RefreshToken)
     *
     * @param
     * @date 2018/9/6 9:58
     */
    @RequiresAuthentication
    @GetMapping("/online")
    public Object online() {
        List<Object> userDtos = new ArrayList<Object>();
        // 查询所有Redis键
        Set<String> keys = JedisUtil.keysS(Constant.PREFIX_SHIRO_REFRESH_TOKEN + "*");
        for (String key : keys) {
            if (JedisUtil.exists(key)) {
                // 根据:分割key，获取最后一个字符(帐号)
                String[] strArray = key.split(":");
                User userDto = new User();
                userDto.setAccount(strArray[strArray.length - 1]);
                userDto = userService.query().eq("account", userDto.getAccount()).one();
                // 设置登录时间
                userDtos.add(userDto);
            }
        }
        if (userDtos == null || userDtos.size() < 0) {
            throw new CustomException("查询失败(Query Failure)");
        }
        return RespUtils.success(userDtos, "查询成功(Query was successful)");
    }

    /**
     * 登录授权
     *
     * @param user
     * @return Object
     * @author muql
     * @date 2018/8/30 16:21
     */
    @PostMapping("/login")
    public Object login(@Validated(UserLoginValidGroup.class) @RequestBody User user, HttpServletResponse httpServletResponse) {
        // 查询数据库中的帐号信息
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(user.getAccount(), user.getPassword());
        try {
            subject.login(usernamePasswordToken);
        } catch (UnknownAccountException e) {
            return RespUtils.fail(null, "该帐号不存在");
        } catch (IncorrectCredentialsException e) {
            return RespUtils.fail(null, "密码错误");
        }
        Object principal = SecurityUtils.getSubject().getPrincipal();
        httpServletResponse.setHeader("Authorization", principal.toString());
        httpServletResponse.setHeader("Access-Control-Expose-Headers", "Authorization");
        return RespUtils.success(null, "登录成功(Login Success.");
    }

    @RequiresAuthentication
    @PostMapping("/logout")
    public Object logout() {
        // 查询数据库中的帐号信息
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return RespUtils.success(null, "登处成功(Logout Success.)");
    }

    /**
     * 获取当前登录用户信息
     *
     * @param
     * @return Object
     * @author muql
     * @date 2019/3/15 11:51
     */
    @RequiresAuthentication
    @GetMapping("/info")
    public Object info() {
        // 获取当前登录用户
        User userDto = userUtil.getUser();
        return RespUtils.success(userDto, null);
    }

    /**
     * 修改密码
     *
     * @param
     * @return Object
     * @author muql
     * @date 2019/3/15 11:51
     */
    @RequiresAuthentication
    @PostMapping("/modify/pass")
    public Object modifyPass(@RequestParam String oldPass, @RequestParam String newPass) {
        // 获取当前登录用户
        User userDto = userUtil.getUser();
        if (!Objects.equals(MD5Util.getMD5(oldPass), userDto.getPassword())) {
            return RespUtils.fail(null, "原密码错误，请重新输入");
        }
        userDto.setPassword(MD5Util.getMD5(newPass));
        boolean update = userService.updateById(userDto);
        if (update) {
            return RespUtils.success(userDto, null);
        }else{
            return RespUtils.fail(null, "服务器异常，请稍后再试");
        }
    }

    /**
     * 修改个人信息
     *
     * @param
     * @return Object
     * @author muql
     * @date 2019/3/15 11:51
     */
    @RequiresAuthentication
    @PostMapping("/modify/info")
    public Object modifyInfo(@RequestParam String username) {
        // 获取当前登录用户
        User userDto = userUtil.getUser();
        userDto.setUsername(username);
        boolean update = userService.updateById(userDto);
        if (update) {
            return RespUtils.success(userDto, null);
        }else{
            return RespUtils.fail(null, "服务器异常，请稍后再试");
        }
    }

    /**
     * 获取指定用户
     *
     * @param id
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author muql
     * @date 2018/8/30 10:42
     */
    @RequiresAuthentication
    @GetMapping("/{id}")
    public Object findById(@PathVariable("id") Integer id) {
        User userDto = userService.getById(id);
        if (userDto == null) {
            throw new CustomException("查询失败(Query Failure)");
        }
        return RespUtils.success(userDto, "查询成功(Query was successful)");
    }

    /**
     * 新增用户
     *
     * @param userDto
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author muql
     * @date 2018/8/30 10:42
     */

    @PostMapping(value = "/add")
    public Object add(@Validated(UserEditValidGroup.class) @RequestBody User userDto) {
        // 判断当前帐号是否存在
        User userDtoTemp = userService.getOne(new QueryWrapper<User>().eq("account", userDto.getAccount()));
        if (userDtoTemp != null && StringUtil.isNotBlank(userDtoTemp.getPassword())) {
            throw new CustomUnauthorizedException("该帐号已存在(Account exist.)");
        }
        userDto.setRegTime(new Date());
        // 密码以帐号+密码的形式进行AES加密
        if (userDto.getPassword().length() >= Constant.PASSWORD_MAX_LEN) {
            throw new CustomException("密码最多" + Constant.PASSWORD_MAX_LEN + "位");
        }
        userDto.setPassword(MD5Util.getMD5(userDto.getPassword()));
        boolean save = userService.save(userDto);
        if (!save) {
            throw new CustomException("新增失败(Insert Failure)");
        }
        return RespUtils.success(userDto, "新增成功(Insert Success)");
    }

    /**
     * 更新用户
     *
     * @param userDto
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author muql
     * @date 2018/8/30 10:42
     */
    @RequiresAuthentication
    @PutMapping
//    @RequiresPermissions(logical = Logical.AND, value = {"user:edit"})
    public ResponseBean update(@Validated(UserEditValidGroup.class) @RequestBody User userDto) {
        // 查询数据库密码
        User userDtoTemp = userService.getOne(new QueryWrapper<User>().eq("account", userDto.getAccount()));
        if (userDtoTemp == null) {
            throw new CustomUnauthorizedException("该帐号不存在(Account not exist.)");
        } else {
            userDto.setId(userDtoTemp.getId());
        }
        // FIXME: 如果不一样就说明用户修改了密码，重新加密密码(这个处理不太好，但是没有想到好的处理方式)
        if (!userDtoTemp.getPassword().equals(userDto.getPassword())) {
            // 密码以帐号+密码的形式进行AES加密
            if (userDto.getPassword().length() > Constant.PASSWORD_MAX_LEN) {
                throw new CustomException("密码最多8位(Password up to 8 bits.)");
            }
        }
        boolean count = userService.updateById(userDto);
        if (count) {
            throw new CustomException("更新失败(Update Failure)");
        }
        return new ResponseBean(HttpStatus.OK.value(), "更新成功(Update Success)", userDto);
    }

    /**
     * 删除用户
     *
     * @param id
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author muql
     * @date 2018/8/30 10:43
     */
    @RequiresAuthentication
    @DeleteMapping("/{id}")
    public ResponseBean delete(@PathVariable("id") Integer id) {
        boolean count = userService.removeById(id);
        if (count) {
            throw new CustomException("删除失败，ID不存在(Deletion Failed. ID does not exist.)");
        }
        return new ResponseBean(HttpStatus.OK.value(), "删除成功(Delete Success)", null);
    }

    /**
     * 剔除在线用户
     *
     * @param id
     * @return Object
     * @author muql
     * @date 2018/9/6 10:20
     */
    @RequiresAuthentication
    @DeleteMapping("/online/{id}")
    public ResponseBean deleteOnline(@PathVariable("id") Integer id) {
        User userDto = userService.getById(id);
        if (JedisUtil.exists(Constant.PREFIX_SHIRO_REFRESH_TOKEN + userDto.getAccount())) {
            if (JedisUtil.delKey(Constant.PREFIX_SHIRO_REFRESH_TOKEN + userDto.getAccount()) > 0) {
                return new ResponseBean(HttpStatus.OK.value(), "剔除成功(Delete Success)", null);
            }
        }
        throw new CustomException("剔除失败，Account不存在(Deletion Failed. Account does not exist.)");
    }
}