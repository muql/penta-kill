new Vue({
    el: '#sigUp',
    data: {
        username:"",
        account:"",
        password:"",
        password2:""
    },
    methods: {
        sigup() {
            if(!(this.password==this.password2)){
                alert("两次密码不一致，请重新输入");
                return;
            }
            if(this.username==""){
                alert("请输入昵称");
                return;
            }
            if(this.account==""){
                alert("请输入账号");
                return;
            }
            axios({
                method: 'post',
                url: '/user/add',
                data: {
                    username:　this.username,
                    account: this.account,
                    password: this.password
                },
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function (response) {
                if (response.data.code == 0) {
                    console.log(response)
                    window.location.href="/user/index"
                }
            });
        }
    }
})
