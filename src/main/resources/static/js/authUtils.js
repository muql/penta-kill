function saveAuth(auth) {
    if (auth == undefined||auth == null) {
        return
    }
    var storage = window.localStorage;
    storage['auth'] = auth;
}

function getAuth() {
    var storage = window.localStorage;
    return storage['auth'];
}