new Vue({
    el: '#login',
    data: {
        account: "",
        password: ""
    },
    methods: {
        login: function () {
            axios({
                method: 'post',
                url: '/user/login',
                data: {
                    account: this.account,
                    password: this.password
                },
                headers: {
                    'Content-type': 'application/json'
                }
            }).then(function (response) {
                if (response.data.code == 0) {
                    saveAuth(response.headers.authorization)
                    window.location.href="/user/home"
                }else{
                    alert(response.data.msg)
                }
            });
        },
        toSignUp: function () {
            window.location.href="/user/toSignUp"
        }
    }
})
